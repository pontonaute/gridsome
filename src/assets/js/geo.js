import 'ol/ol.css';
import { Overlay, Feature, Map, View } from 'ol';
import { fromLonLat } from 'ol/proj';
import { Point } from 'ol/geom';
import { Style, Icon } from 'ol/style';
import { Vector as VectorSource, OSM } from 'ol/source';
import { Vector as VectorLayer, Tile } from 'ol/layer';
import $ from 'jquery';

var url = 'https://pontonaute-api.herokuapp.com/api/coordonnees'; // url de la source des données
var path = __dirname + 'assets/img/icon';
/**
 * Elements that make up the popup.
 */
var container = document.getElementById('popup');
var content = document.getElementById('popup-content');

/**
 * Create an overlay to anchor the popup to the map.
 */
var overlay = new Overlay({
  element: container,
  autoPan: true,
  autoPanAnimation: {
    duration: 250,
  },
});
$.ajax({
  url,
  dataType: 'json',
  success: function(data) {
    console.log(data);
    var featuresCollection = data;
    var center = featuresCollection[0].geometry.coordinates;

    // points de la carte
    var features = [];
    featuresCollection.forEach((item, index) => {
      let categories = item.properties.categories;
      let src = `${path}/defaut.png`;
      categories.forEach((cat, i) => {
        let nom = cat.nom;
        if (nom == 'Commerce de bouche') src = `${path}/commerce.png`;
        if (nom == 'Producteur') src = `${path}/producteur.png`;
        if (nom == 'Loisirs et bien-être') src = `${path}/loisir.png`;
        if (nom == 'Services') src = `${path}/service.png`;
        if (nom == 'Soin') src = `${path}/soin.png`;
      });
      // chercher si le commerce vend du bio
      const bio = categories.find(element => element == 'Bio');
      if (bio) src = `${path}/bio.png`;

      features.push(
        new Feature({
          geometry: new Point(
            fromLonLat(item.geometry.coordinates, 'EPSG:3857')
          ),
        })
      );
      features[index].setStyle(
        new Style({
          image: new Icon({
            src,
          }),
        })
      );
      features[index].setProperties({
        value: {
          props: item.properties,
          coord: item.geometry.coordinates,
          index: index,
        },
      });
    });

    var vectorSource = new VectorSource({
      features,
    });

    var vectorLayer = new VectorLayer({
      source: vectorSource,
      updateWhileAnimating: true,
      updateWhileInteracting: true,
    });

    // carte
    var map = new Map({
      layers: [
        new Tile({
          source: new OSM(),
        }),
        vectorLayer,
      ],
      overlays: [overlay],
      target: 'map', // id de la div
      view: new View({
        center: fromLonLat(center), // centrer l'affichage
        zoom: 10,
      }),
    });

    /**
     * Modifier la taille de l'icon quand la souris passe sur l'élément
     * @param {String} src
     * @param {Number} scale
     */
    function iconStyle(src, scale) {
      return new Style({
        image: new Icon({
          scale,
          opacity: 1,
          src,
        }),
      });
    }

    var selected = null;
    var src = '';
    var status = document.getElementById('status');

    /**
     * Add a click handler to hide the popup.
     * @return {boolean} Don't follow the href.
     */
    $('#closer').click(() => {
      //overlay.setPosition(undefined);
      $('#closer').blur();
      return false;
    });

    /**
     * Add a click handler to the map to render the popup.
     */
    map.on('singleclick', function(evt) {
      map.forEachFeatureAtPixel(evt.pixel, function(f) {
        //résupérer position du click pour afficher la popoup dessus
        var coordinate = evt.coordinate;
        let val = f.getProperties().value;
        console.log(val);
        //ajout du texte dans la popup
        content.innerHTML = '<p>' + val.props.commerce + '</p>';

        //affichage
        overlay.setPosition(coordinate);
      });
    });

    // Cursor
    map.on('pointermove', function(evt) {
      if (selected !== null) {
        selected.setStyle(iconStyle(src, 1));
        selected = null;
      }

      var hit = this.forEachFeatureAtPixel(evt.pixel, function(f) {
        selected = f;
        src = selected.getStyle().image_.iconImage_.src_; // récupérer la souce de l'icon
        f.setStyle(iconStyle(src, 1.25));
        return true;
      });
      this.getTargetElement().style.cursor = hit ? 'pointer' : '';
    });

    // FIN AJAX
  },
  error: function(jqXHR, textStatus, errorThrown) {
    console.log('ERROR', textStatus, errorThrown);
  },
});

import DefaultLayout from '~/layouts/Default.vue';
import '~/assets/sass/style.scss';

export default function(Vue) {
  Vue.component('Layout', DefaultLayout);
}
